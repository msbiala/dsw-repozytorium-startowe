# DSW Workshop dev
### LAPTOP

1. Zainstaluj gita i dockera na swoim laptopie:

* https://docs.docker.com/desktop/
* https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

Na windows jeszcze to (po tym restart komputera):
* https://docs.docker.com/desktop/windows/wsl/

2. Załóż konto na www.gitlab.com
3. Daj gwiazdkę, żebym wiedział kogo dorzucić do projektu.
4. Otwórz terminal (Windowsowcy mogą użyć Git Bash które installuje się razem z gitem)

5. Wykonaj kolejno poniższe komendy (może być wymagane `sudo`)

```{bash}
git clone https://gitlab.com/zielinskipp/dsw-workshop.git
cd dsw-workshop

docker build -t rstudio-workshop .
docker run -d -p 8787:8787 -e PASSWORD=pass -v $(pwd):/home/rstudio --name rstudio rstudio-workshop
```

6. W przeglądarce internetowej pod adresem `localhost:8787` powinno być dostępne RStudio

```{bash}
username: rstudio
password: pass
```

7. Przydatne komendy po robocie

* `docker stop rstudio` - zatrzymuje rstudio, "wyłącza program"
* `docker start rstudio` - uruchamia rstudio, "włącza program"

8. Jeżeli będą jakieś problemy to zapraszam do zakładania Issues https://gitlab.com/zielinskipp/dsw-workshop/-/issues i opisanie problemu.
Ułatwi to ewentualną pomoc. 
